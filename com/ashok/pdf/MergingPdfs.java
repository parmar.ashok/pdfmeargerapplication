package com.ashok.pdf.merger;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;

public class MergingPdfs {

	private String targetPath="/home/ashok/Project/ashok/IT_returns_2010_19.pdf";
	private String sourcePath= "/home/ashok/Project/ashok/";
	
	public static void main(String[] args) throws IOException {
		MergingPdfs pdf = new MergingPdfs();
		pdf.startProcess();

	}
	
	public void startProcess() throws IOException {
		 //Loading an existing PDF document 
	      File file1 = new File(sourcePath+"ITR 10.pdf"); 
	      PDDocument doc1 = PDDocument.load(file1); 

	      File file2 = new File(sourcePath+"ITR 11.pdf");
	      PDDocument doc2 = PDDocument.load(file2); 
	      
	      File file3 = new File(sourcePath+"ITR 12.pdf");
	      PDDocument doc3 = PDDocument.load(file3); 
	      
	      File file4 = new File(sourcePath+"ITR 13.pdf");
	      PDDocument doc4 = PDDocument.load(file4); 
	      
	      File file5 = new File(sourcePath+"ITR 14.pdf");
	      PDDocument doc5 = PDDocument.load(file5); 
	      
	      File file6 = new File(sourcePath+"ITR 15.pdf");
	      PDDocument doc6 = PDDocument.load(file6); 
	      
	      File file7 = new File(sourcePath+"ITR 16.pdf");
	      PDDocument doc7 = PDDocument.load(file7); 
	      
	      File file8 = new File(sourcePath+"ITR 17.pdf");
	      PDDocument doc8 = PDDocument.load(file8); 
	      
	      File file9 = new File(sourcePath+"ITR 18.pdf");
	      PDDocument doc9 = PDDocument.load(file9); 
	     
	      File file10 = new File(sourcePath+"ITR 19- 1.pdf");
	      PDDocument doc10 = PDDocument.load(file10); 
	      

	      //Instantiating PDFMergerUtility class 
	      PDFMergerUtility PDFmerger = new PDFMergerUtility();       

	      //Setting the destination file 
	      PDFmerger.setDestinationFileName(targetPath);
	      //adding the source files 
	      PDFmerger.addSource(file1); 
	      PDFmerger.addSource(file2); 
	      PDFmerger.addSource(file3);
	      PDFmerger.addSource(file4);
	      PDFmerger.addSource(file5);
	      PDFmerger.addSource(file6);
	      PDFmerger.addSource(file7);
	      PDFmerger.addSource(file8);
	      PDFmerger.addSource(file9);
	      PDFmerger.addSource(file10);

	      //Merging the two documents 
	      PDFmerger.mergeDocuments(); 
	      System.out.println("Documents merged"); 

	      //Closing the documents 
	      doc1.close(); 
	      doc2.close(); 
	      doc3.close();
	      doc4.close();
	      doc5.close();
	      doc6.close();
	      doc7.close();
	      doc8.close();
	      doc9.close();
	      doc10.close();
	}
}
